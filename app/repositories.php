<?php
declare(strict_types=1);

use App\Domain\User\IUserRepository;
use App\Infrastructure\Persistence\User\UserRepository;
use App\Application\Interfaces\IUserService;
use App\Application\Services\UserService;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        IUserRepository::class => \DI\autowire(UserRepository::class),
        IUserService::class => \DI\autowire(UserService::class),
    ]);
};
