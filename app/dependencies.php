<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use \Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Extension\DebugExtension;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        // monolog
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        // doctrine
        EntityManager::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');
            $doctrineSettings = $settings['doctrine'];
            $config = Setup::createAnnotationMetadataConfiguration(
                $doctrineSettings['metadata_dirs'],
                $doctrineSettings['dev_mode']
            );
        
            $config->setMetadataDriverImpl(
                new AnnotationDriver(
                    new AnnotationReader,
                    $doctrineSettings['metadata_dirs']
                )
            );
        
            $config->setMetadataCacheImpl(
                new FilesystemCache($doctrineSettings['cache_dir'])
            );
        
            return EntityManager::create($doctrineSettings['connection'], $config);
        },
        // memcached
        "cache" => function (ContainerInterface $c) {
            $settings = $c->get('settings');
            $cacheSettings = $settings['memcached'];
            $memcached = new \Memcached();
            $memcached->addServer($cacheSettings['host'], 11211);

            return $memcached;
        },
        // twig
        Environment::class => function (ContainerInterface $c) : Environment {
            $settings = $c->get('settings');
            $twigSettings = $settings['twig'];
            $loader = new FilesystemLoader($twigSettings['template_dir']);
            $twig = new Environment($loader, [
                $twigSettings['cache_dir']
            ]);

            if ($twigSettings['dev_mode']) {
                $twig->enableDebug();
                $twig->addExtension(new DebugExtension());
            }

            return $twig;
        },
    ]);
};
