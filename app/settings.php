<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'doctrine' => [
                // if true, metadata caching is forcefully disabled
                'dev_mode' => true,
    
                // path where the compiled metadata info will be cached
                // make sure the path exists and it is writable
                'cache_dir' => __DIR__ . '/../var/cache/doctrine',
    
                // you should add any other path containing annotated entity classes
                'metadata_dirs' => [
                    __DIR__ . '/../src/Domain'
                ],
    
                'connection' => [
                    'driver' => 'pdo_mysql',
                    'host' => isset($_ENV['docker']) ? 'mariadb' : '127.0.0.1',
                    'port' => 3306,
                    'dbname' => 'usersdb',
                    'user' => 'slimuser',
                    'password' => 'welcome1',
                    'charset' => 'utf8mb4'
                ]
            ],
            'memcached' => [
                'host' => isset($_ENV['docker']) ? 'memcached' : '127.0.0.1',
            ],
            'twig' => [
                'dev_mode' => true,
                'template_dir' => __DIR__ . '/../templates',
                'cache_dir' => __DIR__ . '/../var/cache/twig',
            ],
        ],
    ]);
};
