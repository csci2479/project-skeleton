<?php
declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Infrastructure\Persistence\User\UserRepository;
use Tests\TestCase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class UserRepositoryTest extends TestCase
{
    public function testFindAll()
    {
        $users = [
            1 => new User(1, 'bill.gates', 'Bill', 'Gates'),
            2 => new User(2, 'steve.jobs', 'Steve', 'Jobs'),
            3 => new User(3, 'mark.zuckerberg', 'Mark', 'Zuckerberg'),
            4 => new User(4, 'evan.spiegel', 'Evan', 'Spiegel'),
            5 => new User(5, 'jack.dorsey', 'Jack', 'Dorsey'),
        ];
        $emMock = $this->createMock(EntityManager::class);
        $repoMock = $this->createMock(EntityRepository::class);
        $repoMock->method('findAll')->willReturn($users);
        $emMock->method('getRepository')->willReturn($repoMock);
        $userRepository = new UserRepository($emMock);

        $this->assertEquals($users, $userRepository->findAll());
    }

    public function testFindUserOfId()
    {
        $user = new User(1, 'bill.gates', 'Bill', 'Gates');
        $emMock = $this->createMock(EntityManager::class);
        $emMock->method('find')->willReturn($user);
        $userRepository = new UserRepository($emMock);

        $this->assertEquals($user, $userRepository->findUserOfId(1));
    }

    public function testFindUserOfIdThrowsNotFoundException()
    {
        $emMock = $this->createMock(EntityManager::class);
        $emMock->method('find')->willReturn(null);
        $userRepository = new UserRepository($emMock);
        $this->expectException(UserNotFoundException::class);
        $userRepository->findUserOfId(1);
    }
}
