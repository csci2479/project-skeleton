<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\IUserRepository;
use Doctrine\ORM\EntityManager;

class UserRepository implements IUserRepository
{
  /**
   * @var User[]
   */
  private $users = [];
  
  /**
   * @var EntityManager
   */
  private $em;

  /**
   * InMemoryUserRepository constructor.
   *
   * @param array|null $users
   */
  public function __construct(EntityManager $em)
  {
    $this->em = $em;
  }

  /**
   * {@inheritdoc}
   */
  public function findAll(): array
  {
    $userRepo = $this->em->getRepository(User::class);
    
    return $userRepo->findAll();
  }

  /**
   * {@inheritdoc}
   */
  public function findUserOfId(int $id): User
  {
    $user = $this->em->find(User::class, $id);

    if (!isset($user)) {
        throw new UserNotFoundException();
    }

    return $user;
  }
}
