<?php
declare(strict_types=1);

namespace App\Application\Interfaces;

use App\Domain\User\User;

interface IUserService
{
    /**
     * @return User[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return User
     * @throws UserNotFoundException
     */
    public function findUserOfId(int $id): User;
}