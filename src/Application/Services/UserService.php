<?php
declare(strict_types=1);

namespace App\Application\Services;

use App\Application\Interfaces\IUserService;
use App\Domain\User\IUserRepository;
use App\Domain\User\User;
use Psr\Log\LoggerInterface;

class UserService implements IUserService
{
  /**
   * @Inject("cache")
   */
  private $cache;

  /**
   * @var IUserRepository
   */
  private $userRepository;

  /**
   * @var LoggerInterface
   */
  private $logger;

  public function __construct(LoggerInterface $logger, IUserRepository $userRepository)
  {
    $this->logger = $logger;
    $this->userRepository = $userRepository;
  }

  /**
   * @return User[]
   */
  public function findAll(): array
  {
    return $this->getUsersFromCache();
  }

  /**
   * @param int $id
   * @return User
   * @throws UserNotFoundException
   */
  public function findUserOfId(int $id): User
  {
    return $this->userRepository->findUserOfId($id);
  }

  private function getUsersFromCache(): array
  {
    // check cache
    $users = $this->cache->get('users');

    // if not in cache, fetch from db and put in cache
    if (!$users) {
      $this->logger->info('nothing found in cache, retreiving users from db...');
      $users = $this->userRepository->findAll();
      $this->cache->set('users', $users);
    } else {
      $this->logger->info('cache hit!!');
    }

    return $users;
  }
}