<?php
declare(strict_types=1);

namespace App\Application\Actions\Home;

use App\Application\Actions\ViewAction;

final class HomeViewAction extends ViewAction
{
	protected function getTemplate(): string
	{
		return 'home.html.twig';
	}

	protected function getModel(): array
	{
		return [];
	}
}
