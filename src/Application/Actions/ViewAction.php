<?php
declare(strict_types=1);

namespace App\Application\Actions;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use Twig\Environment;
use App\Application\Actions\Action;

abstract class ViewAction extends Action
{
  /**
   * @var Twig
   */
  protected $twig;

  public function __construct(LoggerInterface $logger, Environment $twig)
  {
    parent::__construct($logger);
    $this->twig = $twig;
  }

  abstract protected function getTemplate(): string;

  abstract protected function getModel(): array;

  /**
   * {@inheritdoc}
   */
  protected function action(): Response
  {
    $this->response->getBody()->write(
      $this->twig->render(
        $this->getTemplate(),
        $this->getModel()
      )
    );

    return $this->response;
  }
}
