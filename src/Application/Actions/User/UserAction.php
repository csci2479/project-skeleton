<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use App\Application\Actions\Action;
use App\Application\Interfaces\IUserService;
use Psr\Log\LoggerInterface;

abstract class UserAction extends Action
{
    /**
     * @var IUserService
     */
    protected $userService;

    /**
     * @param LoggerInterface $logger
     * @param IUserService  $userService
     */
    public function __construct(LoggerInterface $logger, IUserService $userService)
    {
        parent::__construct($logger);
        $this->userService = $userService;
    }
}
